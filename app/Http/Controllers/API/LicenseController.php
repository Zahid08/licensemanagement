<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\License;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LicenseController extends Controller
{
    public $successStatus = 200;

    //Save Installations Plugin Informations
    public function installPlugin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'ip_details' => 'required',
            'website_url' => 'required',
            'email' => 'email'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $input = $request->all();
        $result=License::where(['website_url'=>$input['website_url']])->first();
        if ($result){
            $updateModel                          = License::find($result->id);
            $updateModel->name                    =!empty($input['name'])?$input['name']:$updateModel->name;
            $updateModel->ip_details              =!empty($input['ip_details'])?$input['ip_details']:$updateModel->ip_details;
            $updateModel->website_url             =!empty($input['website_url'])?$input['website_url']:$updateModel->website_url;
            $updateModel->email                   =!empty($input['email'])?$input['email']:$updateModel->email;
            $updateModel->username                =isset($input['username'])?$input['username']:$updateModel->username;
            $updateModel->image                   =isset($input['image'])?$input['image']:$updateModel->image;
            $updateModel->user_type               =isset($input['user_type'])?$input['user_type']:$updateModel->user_type;
            $updateModel->plugin_activated_status =1;
            $updateModel->plugin_version           =isset($input['plugin_version'])?$input['plugin_version']:'';
            $updateModel->json                     =json_encode($input);

            if ($updateModel->update()) {
                $success['status'] = 200;
                $success['name'] = $updateModel->website_url;
                return response()->json(['success' => $success], $this->successStatus);
            } else {
                return response()->json(['error' => 'Internal Server Error----'], 500);
            }

        }else{
          $model=new License();
          $model->name                      =$input['name'];
          $model->ip_details                =$input['ip_details'];
          $model->website_url               =$input['website_url'];
          $model->email                     =$input['email'];
          $model->username                  =isset($input['username'])?$input['username']:'';
          $model->image                     =isset($input['image'])?$input['image']:'';
          $model->user_type                 =isset($input['user_type'])?$input['user_type']:'';
          $model->plugin_activated_status   =1;
          $model->plugin_version            =isset($input['plugin_version'])?$input['plugin_version']:'';
          $model->json     =json_encode($input);


            if ($model->save()) {
                $success['name'] = $model->website_url;
                return response()->json(['success' => $success], $this->successStatus);
            } else {
                return response()->json(['error' => 'Internal Server Error----'], 500);
            }

        }
    }

    //Store License Key
    public function setupLicense(Request $request){
        $validator = Validator::make($request->all(), [
            'license_key' => 'required',
            'ip_details' => 'required',
            'website_url' =>'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $input          = $request->all();
        $result         =License::where(['license_key'=>$input['license_key']])->first();
        $trackInstall   =License::where(['website_url'=>$input['website_url']])->first();

        if ($result){
            $error['status']='used';
            $error['message']='this license key already used.Please removed from this website';
            $error['website']=$input['website_url'];
            return response()->json(['success' => $error], 500);
        }else{
            //FIrst Envato Validations

            //update License
            $updateModel                          = License::find($trackInstall->id);
            $updateModel->license_key =$input['license_key'];
            $updateModel->status =1;

            if ($updateModel->update()) {
                $success['status'] = 200;
                $success['name'] = $updateModel->website_url;
                return response()->json(['success' => $success], $this->successStatus);
            } else {
                return response()->json(['error' => 'Internal Server Error----'], 500);
            }
        }
    }

    //Delete License Key
    public function removedLicenseKey(Request $request){
        $validator = Validator::make($request->all(), [
            'license_key' => 'required',
            'ip_details' => 'required',
            'website_url' =>'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $input          = $request->all();
        $result         =License::where(['license_key'=>$input['license_key']])->first();
        if ($result){
            $deleteModel= License::find($result->id);
            if ($deleteModel->delete()){
                $success['status'] ='delete';
                return response()->json(['success' => $success], $this->successStatus);
            }
        }
    }
}
