<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $result['totalInstall']       = DB::table('licenses')->count();
        $result['totalLicenseUsed']   = DB::table('licenses')
            ->where('license_key', '<>', '', 'and')
            ->count();

        $result['totalDeactivatedPlugin']   = DB::table('licenses')
            ->where(['plugin_activated_status'=>2])
            ->count();

        return view('admin.dashboard',$result);
    }

    
    public function auth(Request $request)
    {
        $email=$request->post('email');
        $password=$request->post('password');

        // $result=Admin::where(['email'=>$email, 'password'=>$password])->get();
        $result=Admin::where(['email'=>$email])->first();

        if($result){
            if(Hash::check($password,$result->password)){
                $request->session()->put('ADMIN_LOGIN', true);
                $request->session()->put('ADMIN_ID',$result->id);
                return redirect('admin');
            }else{
                $request->session()->flash('error','Please Enter Correct Password');
                return redirect('admin/login');
            }
            
        }else{
            $request->session()->flash('error','Please Enter Valid Email Address');
            return redirect('admin/login');
        }        
        
    }

    /*
    * @ Authintications
    * @ Calling From Frontend
    */
    public function login(Request $request)
    {
        if($request->session()->has('ADMIN_LOGIN')){
            return redirect('admin');
        }

        return view('admin.auth');

    }

    /*
    * @ Return User List
    * @ Calling From API which one set in wordpress
    */
    public function license(){
       $license= DB::table('licenses')
            ->where('license_key', '<>', '', 'and')
            ->get();
        return view('admin.license_list',['license'=>$license]);
    }

    /*
    * @ Return License View
    * @ Calling From applications
    */
    public function licenseView($licenseId){
        $license= DB::table('licenses')
            ->where('license_key', '<>', '', 'and')
            ->get();
        return view('admin.license_list',['license'=>$license]);
    }

    /*
     * @ Return User List
     * @ Calling From API which one set in wordpress
     */
    public function users(){
        $installUsers= DB::table('licenses')->get();
        return view('admin.user_list',['installUsers'=>$installUsers]);
    }

    public function store(){
        echo 1;
        exit();
    }
    
}
